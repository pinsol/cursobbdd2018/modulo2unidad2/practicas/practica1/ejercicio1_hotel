﻿                                /* m2u2 - Practica 1 */ 
                                      /* HOTEL */

CREATE DATABASE m2u2practica1_hotel;
USE m2u2practica1_hotel;

CREATE OR REPLACE TABLE clientes(
  DNI varchar(10) PRIMARY KEY,
  nombre varchar(20),
  apellidos varchar(20),
  telefono varchar(12),
  email varchar(20),
  direccion varchar(20),
  fechaNac date,
  poblacion varchar(20),
  codPostal int,
  Provincia varchar(20),
  Pais varchar(20)
  );

CREATE OR REPLACE TABLE habitacion(
  numHabitacion int PRIMARY KEY,
  idTipo int
  );

CREATE OR REPLACE TABLE tipoHabitacion(
  idTipo int,
  categoria varchar(20),
  descripcion blob,
  precioHab float,
  PRIMARY KEY (idTipo)
);

ALTER TABLE habitacion
ADD CONSTRAINT FK_habitacion_tipoHabitacion_idTipo FOREIGN KEY (idTipo)
REFERENCES tipoHabitacion (idTipo) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE OR REPLACE TABLE reserva(
  codReserva int AUTO_INCREMENT PRIMARY KEY,
  fechaEntrada date,
  fechaSalida date,
  numHabitacion int,
  DNI varchar(10),
  IVA float
  );

CREATE OR REPLACE TABLE gastos(
  codReserva int PRIMARY KEY,
  idGasto int AUTO_INCREMENT UNIQUE KEY,
  descGasto float,
  importeGasto float
  );

ALTER TABLE reserva
ADD CONSTRAINT FK_reserva_clientes_DNI FOREIGN KEY (DNI)
REFERENCES clientes (DNI) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE reserva
ADD CONSTRAINT FK_reserva_habitacion_numHabitacion FOREIGN KEY (numHabitacion)
REFERENCES habitacion (numHabitacion) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE reserva
ADD CONSTRAINT FK_reserva_gastos_codReserva FOREIGN KEY (codReserva)
REFERENCES gastos (codReserva) ON DELETE CASCADE ON UPDATE CASCADE;
