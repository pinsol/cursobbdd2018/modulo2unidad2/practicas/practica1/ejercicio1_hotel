<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $DNI
 * @property string $nombre
 * @property string $apellidos
 * @property string $telefono
 * @property string $email
 * @property string $direccion
 * @property string $fechaNac
 * @property string $poblacion
 * @property int $codPostal
 * @property string $Provincia
 * @property string $Pais
 *
 * @property Reserva[] $reservas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DNI'], 'required'],
            [['fechaNac'], 'safe'],
            [['codPostal'], 'integer'],
            [['DNI'], 'string', 'max' => 10],
            [['nombre', 'apellidos', 'email', 'direccion', 'poblacion', 'Provincia', 'Pais'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 12],
            [['DNI'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DNI' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'fechaNac' => 'Fecha Nac',
            'poblacion' => 'Poblacion',
            'codPostal' => 'Cod Postal',
            'Provincia' => 'Provincia',
            'Pais' => 'Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['DNI' => 'DNI']);
    }
}
