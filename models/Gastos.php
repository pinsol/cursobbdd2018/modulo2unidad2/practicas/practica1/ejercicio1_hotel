<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastos".
 *
 * @property int $codReserva
 * @property int $idGasto
 * @property double $descGasto
 * @property double $importeGasto
 *
 * @property Reserva $reserva
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codReserva'], 'required'],
            [['codReserva'], 'integer'],
            [['descGasto', 'importeGasto'], 'number'],
            [['codReserva'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codReserva' => 'Cod Reserva',
            'idGasto' => 'Id Gasto',
            'descGasto' => 'Desc Gasto',
            'importeGasto' => 'Importe Gasto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserva()
    {
        return $this->hasOne(Reserva::className(), ['codReserva' => 'codReserva']);
    }
}
