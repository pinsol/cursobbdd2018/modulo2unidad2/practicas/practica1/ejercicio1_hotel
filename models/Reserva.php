<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $codReserva
 * @property string $fechaEntrada
 * @property string $fechaSalida
 * @property int $numHabitacion
 * @property string $DNI
 * @property double $IVA
 *
 * @property Clientes $dNI
 * @property Gastos $codReserva0
 * @property Habitacion $numHabitacion0
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaEntrada', 'fechaSalida'], 'safe'],
            [['numHabitacion'], 'integer'],
            [['IVA'], 'number'],
            [['DNI'], 'string', 'max' => 10],
            [['DNI'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['DNI' => 'DNI']],
            [['codReserva'], 'exist', 'skipOnError' => true, 'targetClass' => Gastos::className(), 'targetAttribute' => ['codReserva' => 'codReserva']],
            [['numHabitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Habitacion::className(), 'targetAttribute' => ['numHabitacion' => 'numHabitacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codReserva' => 'Cod Reserva',
            'fechaEntrada' => 'Fecha Entrada',
            'fechaSalida' => 'Fecha Salida',
            'numHabitacion' => 'Num Habitacion',
            'DNI' => 'Dni',
            'IVA' => 'Iva',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDNI()
    {
        return $this->hasOne(Clientes::className(), ['DNI' => 'DNI']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodReserva0()
    {
        return $this->hasOne(Gastos::className(), ['codReserva' => 'codReserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumHabitacion0()
    {
        return $this->hasOne(Habitacion::className(), ['numHabitacion' => 'numHabitacion']);
    }
}
