<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipohabitacion".
 *
 * @property int $idTipo
 * @property string $categoria
 * @property resource $descripcion
 * @property double $precioHab
 *
 * @property Habitacion[] $habitacions
 */
class Tipohabitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipohabitacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTipo'], 'required'],
            [['idTipo'], 'integer'],
            [['descripcion'], 'string'],
            [['precioHab'], 'number'],
            [['categoria'], 'string', 'max' => 20],
            [['idTipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTipo' => 'Id Tipo',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
            'precioHab' => 'Precio Hab',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabitacions()
    {
        return $this->hasMany(Habitacion::className(), ['idTipo' => 'idTipo']);
    }
}
