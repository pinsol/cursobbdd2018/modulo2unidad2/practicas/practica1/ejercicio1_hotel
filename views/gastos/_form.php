<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gastos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codReserva')->textInput() ?>

    <?= $form->field($model, 'descGasto')->textInput() ?>

    <?= $form->field($model, 'importeGasto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
