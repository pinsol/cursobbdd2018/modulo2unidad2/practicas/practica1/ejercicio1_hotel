<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tipohabitacion */

$this->title = 'Create Tipohabitacion';
$this->params['breadcrumbs'][] = ['label' => 'Tipohabitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipohabitacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
