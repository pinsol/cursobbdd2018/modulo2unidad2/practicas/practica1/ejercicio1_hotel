<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipos de Habitación';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipohabitacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Tipo de Habitación', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idTipo',
            'categoria',
            'descripcion',
            'precioHab',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
